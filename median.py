def median(lst):
    lst.sort()
    l = len(lst)
    if (l%2) != 0:
        med = lst[(l-1)//2]
    else:
        med = ((lst[l//2]+lst[(l//2)-1]))/2
    return med
